import styled from 'styled-components';

const Main = styled.main`
	height: 100vh;
	display: grid;
	place-items: center;
`;

const Wrapper = styled.div`
	position: relative;
	width: calc(${props => props.x} * 2rem);
	height: calc(${props => props.y} * 2rem);
	display: grid;
	grid-template-rows: repeat(${props => props.y}, 2rem);
	grid-template-columns: repeat(${props => props.x}, 2rem);
	border: 1px solid gray;
	&:hover {
		border: 1px solid black;
	}
`;

const Box = styled.div`
	border: 1px solid gray;
	display: grid;
	place-items: center;
`;

const Cap = styled.span`
	font-family: serif;
	color: gray;
`;

const Text = styled.p`
	position: absolute;
	${props => (props.right ? 'right: 1rem;' : 'left: 1rem;')}
	${props => (props.right ? 'text-align: right;' : null)}
	top: 0;
	font-size: 2rem;
`;

function App() {
	const rangePlus = (n, plus) => [...Array(n).keys()].map(x => x + plus);
	const x = 8;
	const y = 5;
	// 65 to 90 caps
	const caps = rangePlus(26, 65).map(n => String.fromCharCode(n));
	// 97 to 122 lowercase
	// const lowercase = rangePlus(26, 97).map(n => String.fromCharCode(n));
	// 48 to 57 0-9
	// const numbers = rangePlus(10, 48).map(n => String.fromCharCode(n));
	const text = 'the manufactory';
	// const minusCharacters = (str, arr) => arr.filter(char => !str.includes(char));
	const minusCapsFromLowercase = (str, arr) =>
		arr.filter(char => !str.includes(char.toLowerCase()));
	return (
		<Main>
			<Wrapper x={x} y={y}>
				{Array(x * y)
					.fill()
					.map((zero, i) =>
						i < x * y - x ? (
							<Box key={i} />
						) : (
							<Box key={i}>
								<Cap>
									{minusCapsFromLowercase(text, caps)[Math.abs(x * y - x - i)]}
								</Cap>
							</Box>
						)
					)}
				<Text right>{text}</Text>
			</Wrapper>
		</Main>
	);
}

export default App;
